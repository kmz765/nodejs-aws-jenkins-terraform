resource "aws_key_pair" "kkm-mykeypair" {
  key_name   = "kkm-mykeypair"
  public_key = file(var.PATH_TO_PUBLIC_KEY)
  lifecycle {
    ignore_changes = [public_key]
  }
}

