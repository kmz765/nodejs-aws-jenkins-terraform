# Internet VPC
resource "aws_vpc" "kkm-main" {
  cidr_block           = "10.0.0.0/16"
  instance_tenancy     = "default"
  enable_dns_support   = "true"
  enable_dns_hostnames = "true"
  enable_classiclink   = "false"
  tags = {
    Name = "main"
  }
}

# Subnets
resource "aws_subnet" "kkm-main-public-1" {
  vpc_id                  = aws_vpc.kkm-main.id
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = "true"
  availability_zone       = "ap-northeast-2a"

  tags = {
    Name = "main-public-1"
  }
}

# Internet GW
resource "aws_internet_gateway" "kkm-main-gw" {
  vpc_id = aws_vpc.kkm-main.id

  tags = {
    Name = "main"
  }
}

# route tables
resource "aws_route_table" "kkm-main-public" {
  vpc_id = aws_vpc.kkm-main.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.kkm-main-gw.id
  }

  tags = {
    Name = "main-public-1"
  }
}

# route associations public
resource "aws_route_table_association" "kkm-main-public-1-a" {
  subnet_id      = aws_subnet.kkm-main-public-1.id
  route_table_id = aws_route_table.kkm-main-public.id
}

